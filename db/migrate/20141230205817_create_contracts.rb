class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.references :customer, index: true

      t.timestamps null: false
    end
    add_foreign_key :contracts, :customers
  end
end
