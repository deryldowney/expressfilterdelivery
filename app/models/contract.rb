class Contract < ActiveRecord::Base
  belongs_to :customer, counter_cache: true
  
  # Lets us differentiate between Customer record changes, and just more|changed contracts 
  after_save -> { self.customer.update_column(:contracts_updated_at, Time.now) }
end
