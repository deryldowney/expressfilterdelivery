class ContractsController < ApplicationController
  respond_to :html, :json

  def show
    @contract = Contract.find(params[:id])
    respond_with @contract
  end
end
