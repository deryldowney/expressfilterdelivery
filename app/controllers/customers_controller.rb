class CustomersController < ApplicationController
  respond_to :html, :json

  def show
    @customer = Contract.find(params[:id])
    respond_with @customer
  end

end
