# RSpec
# spec/support/factory_girl.rb
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods

  # additional factory_girl configuration

  config.before(:suite) do
    begin
      DatabaseCleaner.start
      # FactoryGirl.lint causes problems when using callbacks. Since no object exists yet, 
      # after_save in Contract model makes FactoryGirl see 'contract' factory as invalid:
      # contract - undefined method `update_column' for nil:NilClass (NoMethodError)
      #
      # FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end
end
