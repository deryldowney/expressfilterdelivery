FactoryGirl.define do
  factory :customer do
    first_name "Kaitlyn"
    last_name "Downey"
    middle_initial "N"
    address "3549 W KY 10"
    city "Tollesboro"
    state "KY"
    zip_code "41189"
    phone_number "1-606-555-1212"
    email_address "kdowney@example.com"
  end

end
